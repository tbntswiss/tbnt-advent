<!DOCTYPE html>
<html lang="en">
<head>
	<base href="{{ $project_folder or '' }}">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="google" content="notranslate">
	<meta name="HandheldFriendly" content="true">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="Content-Language" content="en">

	{{-- FAVICON --}}
	<link rel="apple-touch-icon" sizes="180x180" href="public/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="public/images/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="public/images/favicon/favicon-16x16.png" sizes="16x16">
	<meta name="theme-color" content="#000">

	{{-- META --}}
	<title>{{ $social['title'] or '' }}</title>
	<meta name="description" content="" />

	{{-- TWITTER --}}
	<meta name="twitter:title" content="{{ $social['title'] or '' }}" />
	<meta name="twitter:description" content="{{ $social['description'] or '' }}" />
	<meta name="twitter:image:src" content="{{ $social['image'] or 'public/images/share/twitter-share.jpg' }}" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="" />
	<meta name="twitter:creator" content="" />

	{{-- FACEBOOK --}}
	<meta property="og:locale" content="en_US" />
	<meta property="og:title" content="{{ $social['title'] or '' }}" />
	<meta property="og:description" content="{{ $social['description'] or '' }}" />
	<meta property="og:image:width" content="880" />
	<meta property="og:image:height" content="280" />
	<meta property="og:image" itemprop="image" content="{{ $social['image'] or 'public/images/share/facebook-share.jpg' }}" />
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="TBNT" />

	{{-- CSS --}}
	<link rel="stylesheet" href="public/css/app.css?_={{ date('U') }}" />

	{{-- TYPEKIT --}}


	{{-- JS --}}
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110466034-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-110466034-1');
	</script>
</head>
<body>
	<main id="app">
		<div class="main-loader"></div>
		<div class="background-loader">
			<div class="background-loader-content">
				<img src="public/images/brand/advent-white@2x.png" alt="" />
			</div>
		</div>
		<div class="main-container">
			@yield('content')
		</div>
		@include('public.includes.config')
	</main>

	@include('public.includes.nojs')
	@include('public.includes.noie')

	<script src="public/js/app.js?_={{ date('U') }}"></script>
</body>
</html>
