<svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 45.2 (43514) - http://www.bohemiancoding.com/sketch -->
    <title>Facebook-color</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Final" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
        <g id="State_1" transform="translate(-1232.000000, -63.000000)" stroke="#FFFFFF" stroke-width="1.4" class="svg svg-facebook {{ $class or '' }}">
            <g id="Group-2" transform="translate(1233.000000, 64.000000)">
                <path d="M8.54611842,16 L0.883077174,16 C0.395224219,16 0,15.6045305 0,15.1168676 L0,0.883070321 C0,0.395283235 0.395286302,0 0.883077174,0 L15.1169849,0 C15.6046516,0 16,0.395283235 16,0.883070321 L16,15.1168676 C16,15.6045926 15.6045895,16 15.1169849,16 L11.0397684,16 L11.0397684,9.80391822 L13.1195071,9.80391822 L13.4309184,7.38919521 L11.0397684,7.38919521 L11.0397684,5.84754713 C11.0397684,5.1484291 11.2339039,4.67200323 12.2364299,4.67200323 L13.5151038,4.67144448 L13.5151038,2.51169685 C13.293962,2.48226945 12.5349278,2.41652342 11.6518507,2.41652342 C9.80827807,2.41652342 8.54611842,3.54184208 8.54611842,5.60840295 L8.54611842,7.38919521 L6.4610406,7.38919521 L6.4610406,9.80391822 L8.54611842,9.80391822 L8.54611842,16 L8.54611842,16 Z" id="Facebook"></path>
            </g>
        </g>
    </g>
</svg>