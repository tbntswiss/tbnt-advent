@extends('public.app', [
	'js' => 'home',
	'menu' => '',
	'social' => [
		'title' => 'L\'avent, a Sketch Ui kit by TBNT ',
		'description' => 'We wish you a Merry Christmas,',
	],
	])

	@section('content')
	<?php
	$today = new Carbon\Carbon();
	// $today = new Carbon\Carbon('2017-12-16');

	$now = (integer) $today->format('Ymd');
	?>

	<div class="hero" id="scene" data-relative-input>
		<div></div>
		<div class="parallax-background" data-parallax-1 data-depth="0.1" style="background-image: url('public/images/background/mountains.svg')"></div>
		<div class="parallax-background" data-parallax-2 data-depth="0.2" style="background-image: url('public/images/background/mountains-2@2x.png')"></div>
		<div class="parallax-background" data-parallax-3 data-depth="0.4" style="background-image: url('public/images/background/tree-purple.svg')"></div>
		<div class="parallax-background" data-parallax-4 data-depth="0.6" style="background-image: url('public/images/background/tree-black-2.svg')"></div>
		{{-- <div class="parallax-background" data-parallax-6 data-depth="0.1" style="background-image: url('public/images/patterns/rain.svg')"></div> --}}
{{-- 		<div class="parallax-background" data-parallax-5 data-depth="0.6" >
			<div class="brand">
				<img src="public/images/brand/advent@2x.png" alt="" />
				<p>A daily UI advent calendar</p>
			</div>
		</div> --}}
	</div>
	<section class="section advent-section">
		<div class="container">
			<div class="columns">
				<div class="column is-6">
					<div class="advent-intro">
						<div class="content">
							<div class="tbnt">
								<a href="https://tbnt.digital" target="_blank">
									<span>
										<svg width="98px" height="24px" viewBox="0 0 98 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
											<!-- Generator: Sketch 47.1 (45422) - http://www.bohemiancoding.com/sketch -->
											<title>Page 1</title>
											<desc>Created with Sketch.</desc>
											<defs>
												<polygon id="path-1" points="3.26080965 0.0318043165 3.26080965 3.21672518 0.0515417743 3.21672518 0.0515417743 0.0318043165 3.26080965 0.0318043165"></polygon>
											</defs>
											<g id="Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<g id="Noël-cover" transform="translate(-139.000000, -1074.000000)">
													<g id="About" transform="translate(139.000000, 1074.000000)">
														<g id="Group">
															<g id="Page-1">
																<path d="M9.40718346,19.8464288 C9.40718346,18.7952633 8.67626184,17.8875799 7.59598622,17.8875799 C6.48366925,17.8875799 5.75274763,18.7952633 5.75274763,19.8464288 C5.75274763,20.9133065 6.48366925,21.805105 7.59598622,21.805105 C8.67626184,21.805105 9.40718346,20.9133065 9.40718346,19.8464288 L9.40718346,19.8464288 Z M5.75274763,23.6843568 L3.32173988,23.6843568 L3.32173988,12.5366158 L5.75274763,12.5366158 L5.75274763,17.1071482 C6.11812231,16.4064863 7.10330749,15.8171914 8.10434109,15.8171914 C10.3127821,15.8171914 11.8381912,17.5531338 11.8381912,19.8464288 C11.8381912,22.1395511 10.3127821,23.8754935 8.10434109,23.8754935 C7.10330749,23.8754935 6.11812231,23.3020835 5.75274763,22.6014216 L5.75274763,23.6843568 Z" id="Fill-1" fill="#FFFFFF"></path>
																<path d="M19.9099053,16.0083971 L19.9099053,23.6842532 L17.479242,23.6842532 L17.479242,22.5856058 C17.0499569,23.3976345 16.1602067,23.8753899 14.9684755,23.8753899 C13.9836348,23.8753899 13.2368648,23.5409439 12.8236003,22.9994763 C12.3471146,22.4101813 12.2356589,21.6616921 12.2356589,20.5630446 L12.2356589,16.0083971 L14.6509905,16.0083971 L14.6509905,19.9419799 C14.6509905,21.0408 14.9846684,21.8050014 16.0015504,21.8050014 C17.0022394,21.8050014 17.479242,21.0408 17.479242,19.8622101 L17.479242,16.0083971 L19.9099053,16.0083971 Z" id="Fill-3" fill="#FFFFFF"></path>
																<path d="M25.852472,23.6842705 C25.4870973,23.7481554 24.9468734,23.811695 24.2318002,23.811695 C23.0881309,23.811695 21.5151766,23.6685583 21.5151766,21.1043568 L21.5151766,17.8241266 L20.3075969,17.8241266 L20.3075969,16.0084144 L21.5151766,16.0084144 L21.5151766,13.7310043 L23.9299914,13.7310043 L23.9299914,16.0084144 L25.6774505,16.0084144 L25.6774505,17.8241266 L23.9299914,17.8241266 L23.9299914,20.5630619 C23.9299914,21.6938245 24.3749526,21.8687309 24.9785702,21.8687309 C25.2805512,21.8687309 25.6300775,21.8530187 25.852472,21.8212489 L25.852472,23.6842705 Z" id="Fill-5" fill="#FFFFFF"></path>
																<path d="M29.8570543,16.0083971 L32.288062,16.0083971 L32.288062,17.1231022 C32.7171748,16.2950158 33.6067528,15.8172604 34.7984841,15.8172604 C35.7836693,15.8172604 36.5304393,16.1517065 36.9435314,16.6931741 C37.4200172,17.2824691 37.5469767,18.0309583 37.5469767,19.1296058 L37.5469767,23.6842532 L35.1163135,23.6842532 L35.1163135,19.7506705 C35.1163135,18.652023 34.7826357,17.8876489 33.7657537,17.8876489 C32.7805685,17.8876489 32.288062,18.652023 32.288062,19.8304403 L32.288062,23.6842532 L29.8570543,23.6842532 L29.8570543,16.0083971 Z" id="Fill-7" fill="#FFFFFF"></path>
																<g id="Group-11" transform="translate(94.401378, 20.610820)">
																	<mask id="mask-2" fill="white">
																		<use xlink:href="#path-1"></use>
																	</mask>
																	<g id="Clip-10"></g>
																	<path d="M1.65602067,0.0318043165 C2.56196382,0.0318043165 3.2608441,0.748351079 3.2608441,1.62443741 C3.2608441,2.50035108 2.56196382,3.21672518 1.65602067,3.21672518 C0.75042205,3.21672518 0.0515417743,2.50035108 0.0515417743,1.62443741 C0.0515417743,0.748351079 0.75042205,0.0318043165 1.65602067,0.0318043165" id="Fill-9" fill="#FFFFFF" mask="url(#mask-2)"></path>
																</g>
																<path d="M44.0540396,19.8305784 C44.0540396,18.7635281 43.323118,17.8717295 42.2109733,17.8717295 C41.1145047,17.8717295 40.3996038,18.7635281 40.3996038,19.8305784 C40.3996038,20.913341 41.1145047,21.7892547 42.2109733,21.7892547 C43.323118,21.7892547 44.0540396,20.913341 44.0540396,19.8305784 M37.968596,19.8305784 C37.968596,17.6327655 39.5730749,15.7854561 42.2109733,15.7854561 C44.8802239,15.7854561 46.4688544,17.6327655 46.4688544,19.8305784 C46.4688544,22.0282187 44.8802239,23.8755281 42.2109733,23.8755281 C39.5730749,23.8755281 37.968596,22.0282187 37.968596,19.8305784" id="Fill-12" fill="#FFFFFF"></path>
																<path d="M7.65939707,11.147741 C7.29402239,11.2114532 6.75379845,11.2749928 6.03872524,11.2749928 C4.89471146,11.2749928 3.32175711,11.1316835 3.32175711,8.56782734 L3.32175711,5.28742446 L2.11417743,5.28742446 L2.11417743,3.47171223 L3.32175711,3.47171223 L3.32175711,1.19447482 L5.73657192,1.19447482 L5.73657192,3.47171223 L7.48437554,3.47171223 L7.48437554,5.28742446 L5.73657192,5.28742446 L5.73657192,8.02635971 C5.73657192,9.1571223 6.18170543,9.33202878 6.78549526,9.33202878 C7.08747631,9.33202878 7.43683032,9.31631655 7.65939707,9.28454676 L7.65939707,11.147741 Z" id="Fill-14" fill="#FFFFFF"></path>
																<path d="M8.046236,8.63309353e-05 L10.3980017,8.63309353e-05 L10.3980017,4.58633094 C10.8269423,3.79035971 11.7004996,3.28066187 12.8765547,3.28066187 C13.8613953,3.28066187 14.544944,3.61510791 14.9580362,4.15657554 C15.434522,4.7458705 15.5776744,5.5102446 15.5776744,6.60889209 L15.5776744,11.1476547 L13.226081,11.1476547 L13.226081,7.21407194 C13.226081,6.1152518 12.8924031,5.3508777 11.8755211,5.3508777 C10.8906804,5.3508777 10.3980017,6.1152518 10.3980017,7.29384173 L10.3980017,11.1476547 L8.046236,11.1476547 L8.046236,8.63309353e-05 Z" id="Fill-16" fill="#FFFFFF"></path>
																<path d="M22.0341258,7.30988201 C22.0341258,6.25854388 21.3035487,5.35086043 20.2069078,5.35086043 C19.1106115,5.35086043 18.3796899,6.25854388 18.3796899,7.30988201 C18.3796899,8.37658705 19.1106115,9.26838561 20.2069078,9.26838561 C21.3035487,9.26838561 22.0341258,8.37658705 22.0341258,7.30988201 L22.0341258,7.30988201 Z M24.4651335,3.47178129 L24.4651335,11.1476374 L22.0341258,11.1476374 L22.0341258,10.0648748 C21.6687511,10.765364 20.6839104,11.3387741 19.698553,11.3387741 C17.4899397,11.3387741 15.9645306,9.60283165 15.9645306,7.30988201 C15.9645306,5.01641439 17.4899397,3.2806446 19.698553,3.2806446 C20.6839104,3.2806446 21.6687511,3.86993957 22.0341258,4.57042878 L22.0341258,3.47178129 L24.4651335,3.47178129 Z" id="Fill-18" fill="#FFFFFF"></path>
																<path d="M24.9814643,3.47178129 L27.412472,3.47178129 L27.412472,4.58648633 C27.8412403,3.7584 28.7315073,3.2806446 29.9228941,3.2806446 C30.9077347,3.2806446 31.6545047,3.61509065 32.0679414,4.15655827 C32.5444272,4.74585324 32.6717313,5.49416978 32.6717313,6.59298993 L32.6717313,11.1478101 L30.2407235,11.1478101 L30.2407235,7.21405468 C30.2407235,6.11540719 29.9070457,5.35103309 28.8901637,5.35103309 C27.9051507,5.35103309 27.412472,6.11540719 27.412472,7.29382446 L27.412472,11.1478101 L24.9814643,11.1478101 L24.9814643,3.47178129 Z" id="Fill-20" fill="#FFFFFF"></path>
																<polygon id="Fill-22" fill="#FFFFFF" points="37.8909388 7.30988201 40.8621705 11.1476374 37.9706977 11.1476374 35.6187597 7.93094676 35.6187597 11.1476374 33.1880965 11.1476374 33.1880965 6.90647482e-05 35.6187597 6.90647482e-05 35.6187597 6.72058705 37.9862016 3.47178129 40.8780189 3.47178129"></polygon>
																<path d="M46.9679242,8.88635396 C46.9679242,10.7017209 45.4108183,11.3547281 43.5043583,11.3547281 C41.3272696,11.3547281 39.976882,10.3512173 39.976882,8.5836777 L42.3601723,8.5836777 C42.3601723,9.39587914 42.8685271,9.71444029 43.5836003,9.71444029 C44.1715418,9.71444029 44.5848062,9.50741871 44.5848062,8.99772086 C44.5848062,8.40859856 43.9808441,8.32900144 42.5348493,7.85107338 C41.263876,7.45326043 40.1674074,7.08669928 40.1674074,5.60560576 C40.1674074,4.01314532 41.4864427,3.26465612 43.5519035,3.26465612 C45.7761929,3.26465612 46.8566408,4.39541871 46.8566408,5.86045468 L44.4575022,5.86045468 C44.4575022,5.2236777 44.1238243,4.85711655 43.4564686,4.85711655 C42.9639621,4.85711655 42.5508699,5.08036835 42.5508699,5.54189353 C42.5508699,6.09959137 43.2182257,6.19490072 44.5052196,6.62482878 C45.8556072,7.02298705 46.9679242,7.38937554 46.9679242,8.88635396" id="Fill-24" fill="#FFFFFF"></path>
																<path d="M49.1486649,11.2534619 C49.1696813,11.7750734 48.6568475,12.1758216 48.0668389,12.1758216 L48.0668389,13.0886849 C49.2990525,13.0886849 50.4690784,12.1758216 50.8267011,10.590095 C51.126615,9.2604259 50.4713178,8.28713094 49.3514212,8.1745554 C48.4821705,8.08718849 47.6740741,8.56425324 47.4749354,9.43343309 C47.2267011,10.516541 48.2342808,11.5906705 49.1486649,11.2534619" id="Fill-26" fill="#FFFFFF"></path>
																<path d="M40.9445306,18.1032863 C40.1407407,18.007459 26.0177433,18.5606676 24.9524548,18.6288691 C24.9534884,18.6597755 24.954522,18.690682 24.9555556,18.7214158 C25.9336779,18.7184806 39.881826,18.2333007 40.8589147,18.199459 C40.8578811,18.1682072 40.9455642,18.1345381 40.9445306,18.1032863 L40.9445306,18.1032863 Z M24.0950904,19.556754 C23.7395349,19.5690129 23.3843239,19.5814446 22.7634798,19.6341065 C23.2999139,19.7080058 23.569509,19.7917468 24.0950904,19.556754 L24.0950904,19.556754 Z M16.822739,20.1812719 C16.0134367,19.9309122 15.6573643,19.9120921 13.1791559,20.2769266 C14.3354005,20.2679482 15.4044789,20.3232 16.822739,20.1812719 L16.822739,20.1812719 Z M11.8664944,20.8798619 C11.2391042,20.7472576 11.2391042,20.7472576 10.1770887,20.9078331 C10.8878553,20.8833151 11.3331611,20.8985094 11.8664944,20.8798619 L11.8664944,20.8798619 Z M10.126615,19.4540201 C9.77260982,19.4975309 9.50680448,19.5375885 9.2416882,19.5779914 C9.24272179,19.6090705 9.33143842,19.6059626 9.33264427,19.6366964 C9.59776055,19.5964662 9.86356589,19.556236 10.0401378,19.518941 C10.1288544,19.5160058 10.1278208,19.4852719 10.126615,19.4540201 L10.126615,19.4540201 Z M9.19896641,20.910941 C8.9255814,20.7346532 8.39621016,20.8461928 7.86528854,20.9264806 C8.30904393,20.9111137 8.66459948,20.898682 9.19896641,20.910941 L9.19896641,20.910941 Z M8.04909561,21.1058763 C7.51593454,21.1243511 6.98432386,21.173905 6.45133506,21.1925525 C6.3622739,21.1954878 6.36347976,21.2262216 6.36347976,21.2262216 C6.89715762,21.2388259 7.34177433,21.2232863 7.87579673,21.235718 L8.04909561,21.1058763 Z M48.9758829,18.8460777 C48.6267011,19.0437755 47.2950904,19.375459 47.929888,19.7250993 C47.0534022,20.0963223 45.9898363,20.1950849 44.9278208,20.3558331 C42.1853575,20.7918043 26.4583979,21.0226532 23.6165375,21.1526676 C22.2840655,21.1991137 20.9555556,21.3690129 19.6229113,21.415459 C18.3807063,21.4897036 17.13764,21.5637755 15.8918174,21.5453007 C12.6906115,21.5636029 9.4878553,21.5511712 6.28544358,21.5387396 C5.75107666,21.5261353 5.30559862,21.5111137 4.85977606,21.4643223 C3.51851852,21.2635165 2.08837209,21.0654734 0.658570198,20.8674302 C1.01136951,20.7931856 1.36365202,20.6880345 1.8044789,20.5796029 C1.09043928,20.5117468 0.467011197,20.4716892 0.0204995693,20.4250705 C0.0129198966,20.2087252 0.00637381568,20.022941 0,19.8375022 C0.355211025,19.8250705 0.889233419,19.8373295 1.33385013,19.8217899 C2.04341085,19.7665381 2.66425495,19.7137036 3.00981912,19.4231137 C3.00878553,19.3923799 3.18415159,19.3243511 3.27235142,19.2899914 C4.51455642,19.2157468 5.39018088,18.8139626 6.72385874,18.7982504 C9.65770887,18.7581928 12.5004307,18.6594302 15.4349699,18.6193727 C18.8125754,18.5637755 22.2794143,18.5050705 25.6523686,18.2947683 C27.0728682,18.2146532 41.4690784,17.8375597 42.8926787,17.8498187 C44.495435,17.8871137 46.1009475,18.0171281 47.6196382,18.211718 C48.4244617,18.3386245 48.962963,18.474682 48.9758829,18.8460777 L48.9758829,18.8460777 Z" id="Fill-28" fill="#FFFFFF"></path>
																<path d="M54.6445823,23.6842705 C54.2792076,23.7481554 53.7389836,23.811695 53.0239104,23.811695 C51.8797244,23.811695 50.3069423,23.6683856 50.3069423,21.1043568 L50.3069423,17.8241266 L49.0993626,17.8241266 L49.0993626,16.0084144 L50.3069423,16.0084144 L50.3069423,13.7310043 L52.7217571,13.7310043 L52.7217571,16.0084144 L54.4695607,16.0084144 L54.4695607,17.8241266 L52.7217571,17.8241266 L52.7217571,20.5630619 C52.7217571,21.6938245 53.1668906,21.8687309 53.7706804,21.8687309 C54.0726615,21.8687309 54.4218432,21.8530187 54.6445823,21.8212489 L54.6445823,23.6842705 Z" id="Fill-30" fill="#FFFFFF"></path>
																<path d="M55.0313351,12.5366158 L57.3831008,12.5366158 L57.3831008,17.1230331 C57.8122136,16.3270619 58.6857709,15.8171914 59.8616537,15.8171914 C60.8466667,15.8171914 61.5302153,16.1516374 61.9431352,16.693105 C62.4197933,17.2824 62.5627735,18.0467741 62.5627735,19.1455942 L62.5627735,23.6841842 L60.21118,23.6841842 L60.21118,19.7506014 C60.21118,18.651954 59.8776744,17.8875799 58.8606202,17.8875799 C57.8757795,17.8875799 57.3831008,18.651954 57.3831008,19.8303712 L57.3831008,23.6841842 L55.0313351,23.6841842 L55.0313351,12.5366158 Z" id="Fill-32" fill="#FFFFFF"></path>
																<path d="M69.0193109,19.8464288 C69.0193109,18.7952633 68.2887339,17.8875799 67.192093,17.8875799 C66.0957967,17.8875799 65.3648751,18.7952633 65.3648751,19.8464288 C65.3648751,20.9133065 66.0957967,21.805105 67.192093,21.805105 C68.2887339,21.805105 69.0193109,20.9133065 69.0193109,19.8464288 L69.0193109,19.8464288 Z M71.4503187,16.0083281 L71.4503187,23.6843568 L69.0193109,23.6843568 L69.0193109,22.6014216 C68.6539363,23.3020835 67.6690956,23.8754935 66.6837382,23.8754935 C64.4751249,23.8754935 62.949888,22.1395511 62.949888,19.8464288 C62.949888,17.5531338 64.4751249,15.8171914 66.6837382,15.8171914 C67.6690956,15.8171914 68.6539363,16.4064863 69.0193109,17.1071482 L69.0193109,16.0083281 L71.4503187,16.0083281 Z" id="Fill-34" fill="#FFFFFF"></path>
																<path d="M71.9666494,16.0083971 L74.3976572,16.0083971 L74.3976572,17.1231022 C74.8264255,16.2950158 75.7166925,15.8172604 76.9080792,15.8172604 C77.8929199,15.8172604 78.6396899,16.1517065 79.0531266,16.6931741 C79.5296124,17.2824691 79.6569165,18.0309583 79.6569165,19.1296058 L79.6569165,23.6842532 L77.2257364,23.6842532 L77.2257364,19.7506705 C77.2257364,18.652023 76.8922308,17.8876489 75.8751766,17.8876489 C74.8903359,17.8876489 74.3976572,18.652023 74.3976572,19.8304403 L74.3976572,23.6842532 L71.9666494,23.6842532 L71.9666494,16.0083971 Z" id="Fill-36" fill="#FFFFFF"></path>
																<polygon id="Fill-38" fill="#FFFFFF" points="84.876124 19.8464288 87.8473557 23.6843568 84.9558829 23.6843568 82.6041171 20.4676662 82.6041171 23.6843568 80.1731094 23.6843568 80.1731094 12.5366158 82.6041171 12.5366158 82.6041171 19.2571338 84.9713867 16.0083281 87.8632041 16.0083281"></polygon>
																<path d="M93.9530233,21.4229698 C93.9530233,23.2383367 92.3959173,23.8913439 90.4894574,23.8913439 C88.3125409,23.8913439 86.9621533,22.8878331 86.9621533,21.1202935 L89.3454436,21.1202935 C89.3454436,21.932495 89.8536262,22.2510561 90.5686994,22.2510561 C91.1568131,22.2510561 91.5699053,22.0440345 91.5699053,21.5343367 C91.5699053,20.9452144 90.9659432,20.8656173 89.5201206,20.3876892 C88.2491473,19.9897036 87.1525065,19.6233151 87.1525065,18.1422216 C87.1525065,16.5497612 88.4715418,15.8012719 90.5371748,15.8012719 C92.761292,15.8012719 93.8417399,16.9320345 93.8417399,18.3970705 L91.4427735,18.3970705 C91.4427735,17.7602935 91.1089233,17.3937324 90.4417399,17.3937324 C89.9492334,17.3937324 89.5361413,17.6169842 89.5361413,18.0785094 C89.5361413,18.6360345 90.2033247,18.7315165 91.4903187,19.1612719 C92.8407063,19.5596029 93.9530233,19.9259914 93.9530233,21.4229698" id="Fill-40" fill="#FFFFFF"></path>
															</g>
														</g>
													</g>
												</g>
											</g>
										</svg>
									</span>
								</a>
								<span class="is-size-7">presents</span>
							</div>
							<div class="advent">
								<img src="public/images/brand/advent-white@2x.png" alt="" />
							</div>
							<div class="content">At TBNT we love the Christmas season, synonym of happiness, humongous meals and (sometimes) wonderful presents.
								<br><br>
								We wanted to give back to the world for the daily inspiration and creativity by pouring ours in the mix !
								<br><br>
								Each day, download your free UI sketch
								<br><br>
								Merry Christmas, <br>
								<i>Team TBNT</i>
								<br><br><br>
								<div class="sketch">
									<span>Compatible with : </span>
									<span width="120px">
										<a href="https://sketchapp.com/" target="_blank">
											<svg viewBox="0 0 485 182" xmlns="http://www.w3.org/2000/svg" style="background:#232323">
												<g transform="translate(47.000000, 48.000000)" fill="none" fill-rule="evenodd">
													<path d="M128.188992,56.8617138 L128.11634,55.89 L127.141914,55.89 L121.093914,55.89 L120,55.89 L120.044795,56.9829967 C120.543867,69.160365 128.753532,77.226 143.353914,77.226 C157.279143,77.226 166.747914,69.9911483 166.747914,59.04 C166.747914,53.1184522 164.693534,48.927976 160.778833,46.0661622 C157.767516,43.8647614 154.166963,42.6239073 148.19641,41.3293443 C147.797486,41.2428476 147.381679,41.1543122 146.883403,41.0494613 C146.587521,40.9871995 145.525896,40.7649683 145.582237,40.77678 C134.739308,38.5036128 131.299914,36.5289237 131.299914,30.648 C131.299914,24.5666916 136.273043,20.694 144.613914,20.694 C152.728938,20.694 157.857573,25.6502434 158.096406,33.4521278 L158.127565,34.47 L159.145914,34.47 L165.109914,34.47 L166.184425,34.47 L166.159635,33.3957757 C165.87899,21.2345154 157.719276,13.05 144.529914,13.05 C131.603206,13.05 123.067914,20.8283391 123.067914,30.9 C123.067914,36.8048062 125.120118,40.9168746 129.040993,43.6642805 C132.027939,45.7572714 135.488151,46.8668408 141.592112,48.0992319 C141.87798,48.1569488 142.176238,48.2165293 142.531319,48.2869684 C142.751113,48.3305699 142.99935,48.379656 143.465218,48.471754 C149.240597,49.6144477 151.788878,50.3197385 154.113641,51.6715769 C156.999603,53.3497493 158.431914,55.7468655 158.431914,59.46 C158.431914,65.9068018 153.01151,69.582 143.521914,69.582 C134.437247,69.582 128.799402,65.0259561 128.188992,56.8617138 Z M183.757914,12 L183.757914,10.95 L182.707914,10.95 L176.743914,10.95 L175.693914,10.95 L175.693914,12 L175.693914,75 L175.693914,76.05 L176.743914,76.05 L182.707914,76.05 L183.757914,76.05 L183.757914,75 L183.757914,63.7670749 L192.246333,55.5000934 L207.405119,75.6316029 L207.720167,76.05 L208.243914,76.05 L215.635914,76.05 L217.751388,76.05 L216.472208,74.3650882 L197.959461,49.9804575 L215.032611,32.9882236 L216.835378,31.194 L214.291914,31.194 L206.311914,31.194 L205.874807,31.194 L205.566824,31.5041754 L183.757914,53.4683087 L183.757914,12 Z M259.105914,56.394 L260.092338,56.394 L260.153869,55.4094972 C260.164692,55.2363423 260.180741,54.9501209 260.19678,54.6052845 C260.209476,54.3323225 260.219947,54.0663344 260.227431,53.8148552 C260.235542,53.5423343 260.239914,53.2944351 260.239914,53.076 C260.239914,39.8967033 251.510612,30.186 238.609914,30.186 C225.733454,30.186 216.559914,40.5227549 216.559914,53.58 C216.559914,67.0161336 225.317654,77.058 238.693914,77.058 C248.808096,77.058 256.621781,71.0568937 259.437034,61.9542432 L259.857728,60.594 L258.433914,60.594 L252.217914,60.594 L251.476351,60.594 L251.228365,61.2928697 C249.460493,66.2750553 244.674125,69.582 238.777914,69.582 C231.069511,69.582 225.695993,64.2882888 224.727978,56.394 L259.105914,56.394 Z M224.972323,49.422 C226.333447,42.3536376 231.531935,37.662 238.609914,37.662 C245.90509,37.662 250.939644,42.199681 252.040866,49.422 L224.972323,49.422 Z M286.699914,38.334 L287.749914,38.334 L287.749914,37.284 L287.749914,32.244 L287.749914,31.194 L286.699914,31.194 L277.921914,31.194 L277.921914,20.064 L277.921914,19.014 L276.871914,19.014 L270.907914,19.014 L269.857914,19.014 L269.857914,20.064 L269.857914,31.194 L262.759914,31.194 L261.709914,31.194 L261.709914,32.244 L261.709914,37.284 L261.709914,38.334 L262.759914,38.334 L269.857914,38.334 L269.857914,63.492 C269.857914,71.7449923 274.138534,76.218 281.995914,76.218 C283.367352,76.218 284.549613,76.1358985 285.542361,75.9980169 C286.155788,75.9128186 286.579398,75.8245666 286.81294,75.7596938 L287.581914,75.5460899 L287.581914,74.748 L287.581914,69.372 L287.581914,68.0692146 L286.308863,68.3459648 C286.257979,68.3570265 286.1549,68.3780018 286.007661,68.4056092 C285.758175,68.4523879 285.48028,68.4993296 285.182145,68.5431729 C284.418645,68.6554524 283.661373,68.7274153 282.964187,68.7400455 C282.892473,68.7413447 282.8217,68.742 282.751914,68.742 C279.309872,68.742 277.921914,67.1252576 277.921914,63.072 L277.921914,38.334 L286.699914,38.334 Z M311.773914,77.058 C322.935724,77.058 330.816731,69.9948331 332.795914,59.9999604 L333.044223,58.746 L331.765914,58.746 L325.717914,58.746 L324.910564,58.746 L324.703155,59.5262538 C323.007659,65.9045484 318.405708,69.582 311.773914,69.582 C303.497642,69.582 297.787914,63.0533472 297.787914,53.664 C297.787914,44.2020218 303.487725,37.662 311.773914,37.662 C318.405708,37.662 323.007659,41.3394516 324.703155,47.7177462 L324.910564,48.498 L325.717914,48.498 L331.765914,48.498 L333.044223,48.498 L332.795914,47.2440396 C330.816731,37.2491669 322.935724,30.186 311.773914,30.186 C298.807945,30.186 289.639914,40.5958108 289.639914,53.664 C289.639914,66.660313 298.820569,77.058 311.773914,77.058 Z M347.893914,12 L347.893914,10.95 L346.843914,10.95 L340.879914,10.95 L339.829914,10.95 L339.829914,12 L339.829914,75 L339.829914,76.05 L340.879914,76.05 L346.843914,76.05 L347.893914,76.05 L347.893914,75 L347.893914,49.716 C347.893914,42.0124523 352.582874,37.494 359.695914,37.494 C366.489563,37.494 369.901914,41.4268794 369.901914,48.876 L369.901914,75 L369.901914,76.05 L370.951914,76.05 L376.915914,76.05 L377.965914,76.05 L377.965914,75 L377.965914,47.7 C377.965914,37.1776416 371.450406,30.186 360.703914,30.186 C355.175195,30.186 350.731176,32.2115513 347.893914,35.1187318 L347.893914,12 Z" fill="#FFFFFF"/>
													<path d="M20.9032258,2.83147321 L47.9032258,0 L74.9032258,2.83147321 L95.8064516,30.6662946 L47.9032258,86 L0,30.6662946 L20.9032258,2.83147321 Z" fill="#FDB300"/>
													<g transform="translate(0.000000, 30.666295)">
														<polygon fill="#EA6C00" points="19.403 0 47.903 55.334 0 0"/>
														<polygon fill="#EA6C00" transform="translate(71.854839, 27.666853) scale(-1, 1) translate(-71.854839, -27.666853)" points="67.306 0 95.806 55.334 47.903 0"/>
														<polygon fill="#FDAD00" points="19.403 0 76.403 0 47.903 55.334"/>
													</g>
													<polygon fill="#FDD231" points="47.903 0 20.903 2.831 19.403 30.666"/>
													<polygon fill="#FDD231" transform="translate(62.153226, 15.333147) scale(-1, 1) translate(-62.153226, -15.333147)" points="76.403 0 49.403 2.831 47.903 30.666"/>
													<polygon fill="#FDAD00" transform="translate(85.354839, 16.748884) scale(-1, 1) translate(-85.354839, -16.748884)" points="74.903 30.666 95.806 2.831 94.306 30.666"/>
													<polygon fill="#FDAD00" points="0 30.666 20.903 2.831 19.403 30.666"/>
													<polygon fill="#FEEEB7" points="47.903 0 19.403 30.666 76.403 30.666"/>
												</g>
											</svg>
										</a>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="advent-items">
				<!-- START ITEM -->
				<div class="advent-item-container advent-item-fake " style="opacity:0; transform: 0">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20771225) is-past @elseif ($now === 20771225) is-present @else is-futur @endif">
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Download All</div>
								<div class="advent-title">24 UI KIT</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171201) is-past @elseif ($now === 20171201) is-present @else is-futur @endif">

						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 01</div>
								<div class="advent-title">Profile</div>
								@if ($now >= 20171201)
								<a href="public/gifts/1/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/1/image@2x.png')"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171202) is-past @elseif ($now === 20171202) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/2/image@2x.png'); background-position: left center;"></div>
						<div class="advent-content">

							<div class="advent-info">
								<div class="advent-date">Day 02</div>
								<div class="advent-title">Search and auto—complete</div>

								@if ($now >= 20171202)
								<a href="public/gifts/2/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171203) is-past @elseif ($now === 20171203) is-present @else is-futur @endif">

						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 03</div>
								<div class="advent-title">WYSIWYG editor</div>

								@if ($now >= 20171203)
								<a href="public/gifts/3/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/3/image@2x.png'); background-position: top right;"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171204) is-past @elseif ($now === 20171204) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/4/image@2x.png');background-position: left bottom;"></div>
						<div class="advent-content">

							<div class="advent-info">
								<div class="advent-date">Day 04</div>
								<div class="advent-title">Form</div>

								@if ($now >= 20171204)
								<a href="public/gifts/4/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171205) is-past @elseif ($now === 20171205) is-present @else is-futur @endif">

						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 05</div>
								<div class="advent-title">Wallet</div>

								@if ($now >= 20171205)
								<a href="public/gifts/3/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/5/image@2x.png'); background-position: right center"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171206) is-past @elseif ($now === 20171206) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/6/image@2x.png'); background-position: left center;"></div>
						<div class="advent-content">

							<div class="advent-info">
								<div class="advent-date">Day 06</div>
								<div class="advent-title">Review</div>

								@if ($now >= 20171206)
								<a href="public/gifts/6/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171207) is-past @elseif ($now === 20171207) is-present @else is-futur @endif">

						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 07</div>
								<div class="advent-title">Date picker</div>

								@if ($now >= 20171207)
								<a href="public/gifts/7/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/7/image@2x.png')"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171208) is-past @elseif ($now === 20171208) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/8/image@2x.png'); background-position: left center;"></div>
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 08</div>
								<div class="advent-title">Quick view</div>
								@if ($now >= 20171208)
								<a href="public/gifts/8/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171209) is-past @elseif ($now === 20171209) is-present @else is-futur @endif">
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 09</div>
								<div class="advent-title">Activity tracker</div>
								@if ($now >= 20171209)
								<a href="public/gifts/9/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/9/image@2x.png')"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171210) is-past @elseif ($now === 20171210) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/10/image@2x.png');background-position: left center;"></div>
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 10</div>
								<div class="advent-title">Sign in</div>

								@if ($now >= 20171210)
								<a href="public/gifts/10/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171211) is-past @elseif ($now === 20171211) is-present @else is-futur @endif">
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 11</div>
								<div class="advent-title">Map and itinerary</div>
								@if ($now >= 20171211)
								<a href="public/gifts/11/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/11/image@2x.png')"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171212) is-past @elseif ($now === 20171212) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/12/image@2x.png')"></div>
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 12</div>
								<div class="advent-title">Playlist</div>

								@if ($now >= 20171212)
								<a href="public/gifts/12/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171213) is-past @elseif ($now === 20171213) is-present @else is-futur @endif">
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 13</div>
								<div class="advent-title">Budget overview</div>
								@if ($now >= 20171213)
								<a href="public/gifts/13/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/13/image@2x.png')"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171214) is-past @elseif ($now === 20171214) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/14/image@2x.png')"></div>
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 14</div>
								<div class="advent-title">Color Palette</div>
								@if ($now >= 20171214)
								<a href="public/gifts/14/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171215) is-past @elseif ($now === 20171215) is-present @else is-futur @endif">
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 15</div>
								<div class="advent-title">Calendar</div>
								@if ($now >= 20171215)
								<a href="public/gifts/15/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/15/image@2x.png')"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171216) is-past @elseif ($now === 20171216) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/16/image@2x.png')"></div>
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 16</div>
								<div class="advent-title">Walk — through</div>
								@if ($now >= 20171216)
								<a href="public/gifts/16/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171217) is-past @elseif ($now === 20171217) is-present @else is-futur @endif">
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 17</div>
								<div class="advent-title">Chat</div>
								@if ($now >= 20171217)
								<a href="public/gifts/17/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/17/image@2x.png')"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171218) is-past @elseif ($now === 20171218) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/18/image@2x.png')"></div>
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 18</div>
								<div class="advent-title">Mobile menu</div>
								@if ($now >= 20171218)
								<a href="public/gifts/18/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->

				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171219) is-past @elseif ($now === 20171219) is-present @else is-futur @endif">
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 19</div>
								<div class="advent-title">Translator</div>
								@if ($now >= 20171219)
								<a href="public/gifts/19/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/19/image@2x.png')"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171220) is-past @elseif ($now === 20171220) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/20/image@2x.png')"></div>
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 20</div>
								<div class="advent-title">Currency converter</div>
								@if ($now >= 20171220)
								<a href="public/gifts/20/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171221) is-past @elseif ($now === 20171221) is-present @else is-futur @endif">
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 21</div>
								<div class="advent-title">E—mail preview</div>
								@if ($now >= 20171221)
								<a href="public/gifts/21/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/21/image@2x.png')"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171222) is-past @elseif ($now === 20171222) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/22/image@2x.png')"></div>
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 22</div>
								<div class="advent-title">Button set</div>
								@if ($now >= 20171222)
								<a href="public/gifts/22/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt is-inverse @if ($now > 20171223) is-past @elseif ($now === 20171223) is-present @else is-futur @endif">
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 23</div>
								<div class="advent-title">Coupon</div>
								@if ($now >= 20171223)
								<a href="public/gifts/23/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
						<div class="advent-image" style="background-image: url('public/gifts/23/image@2x.png')"></div>
					</div>
				</div>
				<!-- END ITEM -->
				<!-- START ITEM -->
				<div class="advent-item-container">
					<div class="advent-item wow-anim fadeIn tilt @if ($now > 20171224) is-past @elseif ($now === 20171224) is-present @else is-futur @endif">
						<div class="advent-image" style="background-image: url('public/gifts/24/image@2x.png')"></div>
						<div class="advent-content">
							<div class="advent-info">
								<div class="advent-date">Day 24</div>
								<div class="advent-title">Full Ui kit</div>
								@if ($now >= 20171224)
								<a href="public/gifts/24/tbnt_lavent_ui.zip" class="advent-download">Download now</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				<!-- END ITEM -->
			</div>
		</div>
	</section>
	<footer class="footer is-inverte">
		<div class="container">
			<div class="columns">
				<div class="column is-4">
					<div class="tbnt">
						<a href="https://tbnt.digital" target="_blank">
							<span>
								<svg width="98px" height="24px" viewBox="0 0 98 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<!-- Generator: Sketch 47.1 (45422) - http://www.bohemiancoding.com/sketch -->
									<title>Page 1</title>
									<desc>Created with Sketch.</desc>
									<defs>
										<polygon id="path-1" points="3.26080965 0.0318043165 3.26080965 3.21672518 0.0515417743 3.21672518 0.0515417743 0.0318043165 3.26080965 0.0318043165"></polygon>
									</defs>
									<g id="Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<g id="Noël-cover" transform="translate(-139.000000, -1074.000000)">
											<g id="About" transform="translate(139.000000, 1074.000000)">
												<g id="Group">
													<g id="Page-1">
														<path d="M9.40718346,19.8464288 C9.40718346,18.7952633 8.67626184,17.8875799 7.59598622,17.8875799 C6.48366925,17.8875799 5.75274763,18.7952633 5.75274763,19.8464288 C5.75274763,20.9133065 6.48366925,21.805105 7.59598622,21.805105 C8.67626184,21.805105 9.40718346,20.9133065 9.40718346,19.8464288 L9.40718346,19.8464288 Z M5.75274763,23.6843568 L3.32173988,23.6843568 L3.32173988,12.5366158 L5.75274763,12.5366158 L5.75274763,17.1071482 C6.11812231,16.4064863 7.10330749,15.8171914 8.10434109,15.8171914 C10.3127821,15.8171914 11.8381912,17.5531338 11.8381912,19.8464288 C11.8381912,22.1395511 10.3127821,23.8754935 8.10434109,23.8754935 C7.10330749,23.8754935 6.11812231,23.3020835 5.75274763,22.6014216 L5.75274763,23.6843568 Z" id="Fill-1" fill="#FFFFFF"></path>
														<path d="M19.9099053,16.0083971 L19.9099053,23.6842532 L17.479242,23.6842532 L17.479242,22.5856058 C17.0499569,23.3976345 16.1602067,23.8753899 14.9684755,23.8753899 C13.9836348,23.8753899 13.2368648,23.5409439 12.8236003,22.9994763 C12.3471146,22.4101813 12.2356589,21.6616921 12.2356589,20.5630446 L12.2356589,16.0083971 L14.6509905,16.0083971 L14.6509905,19.9419799 C14.6509905,21.0408 14.9846684,21.8050014 16.0015504,21.8050014 C17.0022394,21.8050014 17.479242,21.0408 17.479242,19.8622101 L17.479242,16.0083971 L19.9099053,16.0083971 Z" id="Fill-3" fill="#FFFFFF"></path>
														<path d="M25.852472,23.6842705 C25.4870973,23.7481554 24.9468734,23.811695 24.2318002,23.811695 C23.0881309,23.811695 21.5151766,23.6685583 21.5151766,21.1043568 L21.5151766,17.8241266 L20.3075969,17.8241266 L20.3075969,16.0084144 L21.5151766,16.0084144 L21.5151766,13.7310043 L23.9299914,13.7310043 L23.9299914,16.0084144 L25.6774505,16.0084144 L25.6774505,17.8241266 L23.9299914,17.8241266 L23.9299914,20.5630619 C23.9299914,21.6938245 24.3749526,21.8687309 24.9785702,21.8687309 C25.2805512,21.8687309 25.6300775,21.8530187 25.852472,21.8212489 L25.852472,23.6842705 Z" id="Fill-5" fill="#FFFFFF"></path>
														<path d="M29.8570543,16.0083971 L32.288062,16.0083971 L32.288062,17.1231022 C32.7171748,16.2950158 33.6067528,15.8172604 34.7984841,15.8172604 C35.7836693,15.8172604 36.5304393,16.1517065 36.9435314,16.6931741 C37.4200172,17.2824691 37.5469767,18.0309583 37.5469767,19.1296058 L37.5469767,23.6842532 L35.1163135,23.6842532 L35.1163135,19.7506705 C35.1163135,18.652023 34.7826357,17.8876489 33.7657537,17.8876489 C32.7805685,17.8876489 32.288062,18.652023 32.288062,19.8304403 L32.288062,23.6842532 L29.8570543,23.6842532 L29.8570543,16.0083971 Z" id="Fill-7" fill="#FFFFFF"></path>
														<g id="Group-11" transform="translate(94.401378, 20.610820)">
															<mask id="mask-2" fill="white">
																<use xlink:href="#path-1"></use>
															</mask>
															<g id="Clip-10"></g>
															<path d="M1.65602067,0.0318043165 C2.56196382,0.0318043165 3.2608441,0.748351079 3.2608441,1.62443741 C3.2608441,2.50035108 2.56196382,3.21672518 1.65602067,3.21672518 C0.75042205,3.21672518 0.0515417743,2.50035108 0.0515417743,1.62443741 C0.0515417743,0.748351079 0.75042205,0.0318043165 1.65602067,0.0318043165" id="Fill-9" fill="#FFFFFF" mask="url(#mask-2)"></path>
														</g>
														<path d="M44.0540396,19.8305784 C44.0540396,18.7635281 43.323118,17.8717295 42.2109733,17.8717295 C41.1145047,17.8717295 40.3996038,18.7635281 40.3996038,19.8305784 C40.3996038,20.913341 41.1145047,21.7892547 42.2109733,21.7892547 C43.323118,21.7892547 44.0540396,20.913341 44.0540396,19.8305784 M37.968596,19.8305784 C37.968596,17.6327655 39.5730749,15.7854561 42.2109733,15.7854561 C44.8802239,15.7854561 46.4688544,17.6327655 46.4688544,19.8305784 C46.4688544,22.0282187 44.8802239,23.8755281 42.2109733,23.8755281 C39.5730749,23.8755281 37.968596,22.0282187 37.968596,19.8305784" id="Fill-12" fill="#FFFFFF"></path>
														<path d="M7.65939707,11.147741 C7.29402239,11.2114532 6.75379845,11.2749928 6.03872524,11.2749928 C4.89471146,11.2749928 3.32175711,11.1316835 3.32175711,8.56782734 L3.32175711,5.28742446 L2.11417743,5.28742446 L2.11417743,3.47171223 L3.32175711,3.47171223 L3.32175711,1.19447482 L5.73657192,1.19447482 L5.73657192,3.47171223 L7.48437554,3.47171223 L7.48437554,5.28742446 L5.73657192,5.28742446 L5.73657192,8.02635971 C5.73657192,9.1571223 6.18170543,9.33202878 6.78549526,9.33202878 C7.08747631,9.33202878 7.43683032,9.31631655 7.65939707,9.28454676 L7.65939707,11.147741 Z" id="Fill-14" fill="#FFFFFF"></path>
														<path d="M8.046236,8.63309353e-05 L10.3980017,8.63309353e-05 L10.3980017,4.58633094 C10.8269423,3.79035971 11.7004996,3.28066187 12.8765547,3.28066187 C13.8613953,3.28066187 14.544944,3.61510791 14.9580362,4.15657554 C15.434522,4.7458705 15.5776744,5.5102446 15.5776744,6.60889209 L15.5776744,11.1476547 L13.226081,11.1476547 L13.226081,7.21407194 C13.226081,6.1152518 12.8924031,5.3508777 11.8755211,5.3508777 C10.8906804,5.3508777 10.3980017,6.1152518 10.3980017,7.29384173 L10.3980017,11.1476547 L8.046236,11.1476547 L8.046236,8.63309353e-05 Z" id="Fill-16" fill="#FFFFFF"></path>
														<path d="M22.0341258,7.30988201 C22.0341258,6.25854388 21.3035487,5.35086043 20.2069078,5.35086043 C19.1106115,5.35086043 18.3796899,6.25854388 18.3796899,7.30988201 C18.3796899,8.37658705 19.1106115,9.26838561 20.2069078,9.26838561 C21.3035487,9.26838561 22.0341258,8.37658705 22.0341258,7.30988201 L22.0341258,7.30988201 Z M24.4651335,3.47178129 L24.4651335,11.1476374 L22.0341258,11.1476374 L22.0341258,10.0648748 C21.6687511,10.765364 20.6839104,11.3387741 19.698553,11.3387741 C17.4899397,11.3387741 15.9645306,9.60283165 15.9645306,7.30988201 C15.9645306,5.01641439 17.4899397,3.2806446 19.698553,3.2806446 C20.6839104,3.2806446 21.6687511,3.86993957 22.0341258,4.57042878 L22.0341258,3.47178129 L24.4651335,3.47178129 Z" id="Fill-18" fill="#FFFFFF"></path>
														<path d="M24.9814643,3.47178129 L27.412472,3.47178129 L27.412472,4.58648633 C27.8412403,3.7584 28.7315073,3.2806446 29.9228941,3.2806446 C30.9077347,3.2806446 31.6545047,3.61509065 32.0679414,4.15655827 C32.5444272,4.74585324 32.6717313,5.49416978 32.6717313,6.59298993 L32.6717313,11.1478101 L30.2407235,11.1478101 L30.2407235,7.21405468 C30.2407235,6.11540719 29.9070457,5.35103309 28.8901637,5.35103309 C27.9051507,5.35103309 27.412472,6.11540719 27.412472,7.29382446 L27.412472,11.1478101 L24.9814643,11.1478101 L24.9814643,3.47178129 Z" id="Fill-20" fill="#FFFFFF"></path>
														<polygon id="Fill-22" fill="#FFFFFF" points="37.8909388 7.30988201 40.8621705 11.1476374 37.9706977 11.1476374 35.6187597 7.93094676 35.6187597 11.1476374 33.1880965 11.1476374 33.1880965 6.90647482e-05 35.6187597 6.90647482e-05 35.6187597 6.72058705 37.9862016 3.47178129 40.8780189 3.47178129"></polygon>
														<path d="M46.9679242,8.88635396 C46.9679242,10.7017209 45.4108183,11.3547281 43.5043583,11.3547281 C41.3272696,11.3547281 39.976882,10.3512173 39.976882,8.5836777 L42.3601723,8.5836777 C42.3601723,9.39587914 42.8685271,9.71444029 43.5836003,9.71444029 C44.1715418,9.71444029 44.5848062,9.50741871 44.5848062,8.99772086 C44.5848062,8.40859856 43.9808441,8.32900144 42.5348493,7.85107338 C41.263876,7.45326043 40.1674074,7.08669928 40.1674074,5.60560576 C40.1674074,4.01314532 41.4864427,3.26465612 43.5519035,3.26465612 C45.7761929,3.26465612 46.8566408,4.39541871 46.8566408,5.86045468 L44.4575022,5.86045468 C44.4575022,5.2236777 44.1238243,4.85711655 43.4564686,4.85711655 C42.9639621,4.85711655 42.5508699,5.08036835 42.5508699,5.54189353 C42.5508699,6.09959137 43.2182257,6.19490072 44.5052196,6.62482878 C45.8556072,7.02298705 46.9679242,7.38937554 46.9679242,8.88635396" id="Fill-24" fill="#FFFFFF"></path>
														<path d="M49.1486649,11.2534619 C49.1696813,11.7750734 48.6568475,12.1758216 48.0668389,12.1758216 L48.0668389,13.0886849 C49.2990525,13.0886849 50.4690784,12.1758216 50.8267011,10.590095 C51.126615,9.2604259 50.4713178,8.28713094 49.3514212,8.1745554 C48.4821705,8.08718849 47.6740741,8.56425324 47.4749354,9.43343309 C47.2267011,10.516541 48.2342808,11.5906705 49.1486649,11.2534619" id="Fill-26" fill="#FFFFFF"></path>
														<path d="M40.9445306,18.1032863 C40.1407407,18.007459 26.0177433,18.5606676 24.9524548,18.6288691 C24.9534884,18.6597755 24.954522,18.690682 24.9555556,18.7214158 C25.9336779,18.7184806 39.881826,18.2333007 40.8589147,18.199459 C40.8578811,18.1682072 40.9455642,18.1345381 40.9445306,18.1032863 L40.9445306,18.1032863 Z M24.0950904,19.556754 C23.7395349,19.5690129 23.3843239,19.5814446 22.7634798,19.6341065 C23.2999139,19.7080058 23.569509,19.7917468 24.0950904,19.556754 L24.0950904,19.556754 Z M16.822739,20.1812719 C16.0134367,19.9309122 15.6573643,19.9120921 13.1791559,20.2769266 C14.3354005,20.2679482 15.4044789,20.3232 16.822739,20.1812719 L16.822739,20.1812719 Z M11.8664944,20.8798619 C11.2391042,20.7472576 11.2391042,20.7472576 10.1770887,20.9078331 C10.8878553,20.8833151 11.3331611,20.8985094 11.8664944,20.8798619 L11.8664944,20.8798619 Z M10.126615,19.4540201 C9.77260982,19.4975309 9.50680448,19.5375885 9.2416882,19.5779914 C9.24272179,19.6090705 9.33143842,19.6059626 9.33264427,19.6366964 C9.59776055,19.5964662 9.86356589,19.556236 10.0401378,19.518941 C10.1288544,19.5160058 10.1278208,19.4852719 10.126615,19.4540201 L10.126615,19.4540201 Z M9.19896641,20.910941 C8.9255814,20.7346532 8.39621016,20.8461928 7.86528854,20.9264806 C8.30904393,20.9111137 8.66459948,20.898682 9.19896641,20.910941 L9.19896641,20.910941 Z M8.04909561,21.1058763 C7.51593454,21.1243511 6.98432386,21.173905 6.45133506,21.1925525 C6.3622739,21.1954878 6.36347976,21.2262216 6.36347976,21.2262216 C6.89715762,21.2388259 7.34177433,21.2232863 7.87579673,21.235718 L8.04909561,21.1058763 Z M48.9758829,18.8460777 C48.6267011,19.0437755 47.2950904,19.375459 47.929888,19.7250993 C47.0534022,20.0963223 45.9898363,20.1950849 44.9278208,20.3558331 C42.1853575,20.7918043 26.4583979,21.0226532 23.6165375,21.1526676 C22.2840655,21.1991137 20.9555556,21.3690129 19.6229113,21.415459 C18.3807063,21.4897036 17.13764,21.5637755 15.8918174,21.5453007 C12.6906115,21.5636029 9.4878553,21.5511712 6.28544358,21.5387396 C5.75107666,21.5261353 5.30559862,21.5111137 4.85977606,21.4643223 C3.51851852,21.2635165 2.08837209,21.0654734 0.658570198,20.8674302 C1.01136951,20.7931856 1.36365202,20.6880345 1.8044789,20.5796029 C1.09043928,20.5117468 0.467011197,20.4716892 0.0204995693,20.4250705 C0.0129198966,20.2087252 0.00637381568,20.022941 0,19.8375022 C0.355211025,19.8250705 0.889233419,19.8373295 1.33385013,19.8217899 C2.04341085,19.7665381 2.66425495,19.7137036 3.00981912,19.4231137 C3.00878553,19.3923799 3.18415159,19.3243511 3.27235142,19.2899914 C4.51455642,19.2157468 5.39018088,18.8139626 6.72385874,18.7982504 C9.65770887,18.7581928 12.5004307,18.6594302 15.4349699,18.6193727 C18.8125754,18.5637755 22.2794143,18.5050705 25.6523686,18.2947683 C27.0728682,18.2146532 41.4690784,17.8375597 42.8926787,17.8498187 C44.495435,17.8871137 46.1009475,18.0171281 47.6196382,18.211718 C48.4244617,18.3386245 48.962963,18.474682 48.9758829,18.8460777 L48.9758829,18.8460777 Z" id="Fill-28" fill="#FFFFFF"></path>
														<path d="M54.6445823,23.6842705 C54.2792076,23.7481554 53.7389836,23.811695 53.0239104,23.811695 C51.8797244,23.811695 50.3069423,23.6683856 50.3069423,21.1043568 L50.3069423,17.8241266 L49.0993626,17.8241266 L49.0993626,16.0084144 L50.3069423,16.0084144 L50.3069423,13.7310043 L52.7217571,13.7310043 L52.7217571,16.0084144 L54.4695607,16.0084144 L54.4695607,17.8241266 L52.7217571,17.8241266 L52.7217571,20.5630619 C52.7217571,21.6938245 53.1668906,21.8687309 53.7706804,21.8687309 C54.0726615,21.8687309 54.4218432,21.8530187 54.6445823,21.8212489 L54.6445823,23.6842705 Z" id="Fill-30" fill="#FFFFFF"></path>
														<path d="M55.0313351,12.5366158 L57.3831008,12.5366158 L57.3831008,17.1230331 C57.8122136,16.3270619 58.6857709,15.8171914 59.8616537,15.8171914 C60.8466667,15.8171914 61.5302153,16.1516374 61.9431352,16.693105 C62.4197933,17.2824 62.5627735,18.0467741 62.5627735,19.1455942 L62.5627735,23.6841842 L60.21118,23.6841842 L60.21118,19.7506014 C60.21118,18.651954 59.8776744,17.8875799 58.8606202,17.8875799 C57.8757795,17.8875799 57.3831008,18.651954 57.3831008,19.8303712 L57.3831008,23.6841842 L55.0313351,23.6841842 L55.0313351,12.5366158 Z" id="Fill-32" fill="#FFFFFF"></path>
														<path d="M69.0193109,19.8464288 C69.0193109,18.7952633 68.2887339,17.8875799 67.192093,17.8875799 C66.0957967,17.8875799 65.3648751,18.7952633 65.3648751,19.8464288 C65.3648751,20.9133065 66.0957967,21.805105 67.192093,21.805105 C68.2887339,21.805105 69.0193109,20.9133065 69.0193109,19.8464288 L69.0193109,19.8464288 Z M71.4503187,16.0083281 L71.4503187,23.6843568 L69.0193109,23.6843568 L69.0193109,22.6014216 C68.6539363,23.3020835 67.6690956,23.8754935 66.6837382,23.8754935 C64.4751249,23.8754935 62.949888,22.1395511 62.949888,19.8464288 C62.949888,17.5531338 64.4751249,15.8171914 66.6837382,15.8171914 C67.6690956,15.8171914 68.6539363,16.4064863 69.0193109,17.1071482 L69.0193109,16.0083281 L71.4503187,16.0083281 Z" id="Fill-34" fill="#FFFFFF"></path>
														<path d="M71.9666494,16.0083971 L74.3976572,16.0083971 L74.3976572,17.1231022 C74.8264255,16.2950158 75.7166925,15.8172604 76.9080792,15.8172604 C77.8929199,15.8172604 78.6396899,16.1517065 79.0531266,16.6931741 C79.5296124,17.2824691 79.6569165,18.0309583 79.6569165,19.1296058 L79.6569165,23.6842532 L77.2257364,23.6842532 L77.2257364,19.7506705 C77.2257364,18.652023 76.8922308,17.8876489 75.8751766,17.8876489 C74.8903359,17.8876489 74.3976572,18.652023 74.3976572,19.8304403 L74.3976572,23.6842532 L71.9666494,23.6842532 L71.9666494,16.0083971 Z" id="Fill-36" fill="#FFFFFF"></path>
														<polygon id="Fill-38" fill="#FFFFFF" points="84.876124 19.8464288 87.8473557 23.6843568 84.9558829 23.6843568 82.6041171 20.4676662 82.6041171 23.6843568 80.1731094 23.6843568 80.1731094 12.5366158 82.6041171 12.5366158 82.6041171 19.2571338 84.9713867 16.0083281 87.8632041 16.0083281"></polygon>
														<path d="M93.9530233,21.4229698 C93.9530233,23.2383367 92.3959173,23.8913439 90.4894574,23.8913439 C88.3125409,23.8913439 86.9621533,22.8878331 86.9621533,21.1202935 L89.3454436,21.1202935 C89.3454436,21.932495 89.8536262,22.2510561 90.5686994,22.2510561 C91.1568131,22.2510561 91.5699053,22.0440345 91.5699053,21.5343367 C91.5699053,20.9452144 90.9659432,20.8656173 89.5201206,20.3876892 C88.2491473,19.9897036 87.1525065,19.6233151 87.1525065,18.1422216 C87.1525065,16.5497612 88.4715418,15.8012719 90.5371748,15.8012719 C92.761292,15.8012719 93.8417399,16.9320345 93.8417399,18.3970705 L91.4427735,18.3970705 C91.4427735,17.7602935 91.1089233,17.3937324 90.4417399,17.3937324 C89.9492334,17.3937324 89.5361413,17.6169842 89.5361413,18.0785094 C89.5361413,18.6360345 90.2033247,18.7315165 91.4903187,19.1612719 C92.8407063,19.5596029 93.9530233,19.9259914 93.9530233,21.4229698" id="Fill-40" fill="#FFFFFF"></path>
													</g>
												</g>
											</g>
										</g>
									</g>
								</svg>
							</span>
						</a>
					</div>
				</div>
			</div>
			<div class="columns">
				<div class="column is-4">
					<div class="content">
						<h1 class="has-text-white">
							We are millennials and we grew up with the digital transformation.
						</h1>
						<a href="https://tbnt.digital" class="is-link has-text-primary" target="_blank">tbnt.digital</a>
					</div>
				</div>
				<div class="column is-4">
					<div class="is-size-7">
						<div class="content">
							<h2 class="has-text-white">Follow us on</h2>
							<ul>
								<li><a href="https://www.instagram.com/tbnt_/" target="_blank" class="is-link">Instagram</a></li>
								<li><a href="https://dribbble.com/tbnt" target="_blank" class="is-link">Dribbble</a></li>
								<li><a href="https://www.linkedin.com/company-beta/5342361/" target="_blank" class="is-link">Linkedin</a></li>
							</ul>
						</div>

					</div>
				</div>
				<div class="column is-4">
					<div class="is-size-7">
						<div class="content">
							<h4 class="has-text-white">Credits</h4>
							<ul>
								<li>
									<a href="https://ui8.net/users/neway-lau" target="_blank" class="is-link">Caviar Premium Icons by <strong>	Neway Lau</strong></a>
								</li>
								<li>
									<a href="http://unsplash.com" target="_blank" class="is-link">Pictures from <strong>Unsplash.com</strong></a>
								</li>
								<li>
									<a href="https://fonts.google.com/specimen/Montserrat" target="_blank" class="is-link">Montserrat by <strong>Julieta Ulanovsky</strong></a>
								</li>
							</ul>
							<br><br><br><br>
							<small>Copyright Thanks but no thanks 2017 ©</small>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	@endsection
