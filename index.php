<?php

/**
 *
 * @author   Moi
 */

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
*/

require __DIR__.'/vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Configurations
|--------------------------------------------------------------------------
|
*/

$project_folder = '';

$blade_views = __DIR__ . '/views';
$blade_cache = __DIR__ . '/views/cache';

$blade_public = 'public.pages';
$blade_error_404 = 'errors.404';

$homepage = 'home';

/*
|--------------------------------------------------------------------------
| Get page
|--------------------------------------------------------------------------
|
*/

$request_uri = $_SERVER['SCRIPT_URI'] ?? $_SERVER['REDIRECT_URL'] ?? $_SERVER['REQUEST_URI'];

$page = trim(explode('?', trim(str_replace($project_folder, '', $request_uri), '/'))[0]) ?: $homepage;

/*
|--------------------------------------------------------------------------
| Execute page
|--------------------------------------------------------------------------
|
*/

$php_page = 'scripts/'.$page.'.php';

if (file_exists($php_page) === true)
	require_once($php_page);

/*
|--------------------------------------------------------------------------
| Display page
|--------------------------------------------------------------------------
|
*/

use Philo\Blade\Blade;

$blade_page = str_replace('/', '.', $page);

$blade = new Blade($blade_views, $blade_cache);
$blade_options = [
	'project_folder' => $project_folder,
];

try {
	echo $blade->view()->make($blade_public.'.'.$homepage, $blade_options)->render();
}
catch (Exception $e) {
	if (strpos($e->getMessage(), '[public.pages.homes]') === false)
		echo $e->getMessage();
	else
		echo $blade->view()->make($blade_error_404, $blade_options)->render();
}
