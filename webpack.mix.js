const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix options
 |--------------------------------------------------------------------------
 |
 */

mix.options({
	processCssUrls: false,
});

mix.webpackConfig({
	module: {
		loaders: [
			{
				test:   /\.s[ac]ss$/,
				loader: 'style!css!sass'
			}
		]
	}
});



/*
 |--------------------------------------------------------------------------
 | App Asset Management
 |--------------------------------------------------------------------------
 |
 */

// Styles
mix.sass(
	'assets/css/app.sass',
	'assets/build/css/custom.css'
);

mix.styles(
	[
		'node_modules/reset-css/reset.css',
		'node_modules/nprogress/nprogress.css',
		'node_modules/animate.css/animate.min.css',
		'assets/build/css/custom.css',
	],
	'public/css/app.css'
);

// Scripts
mix.js(
	'assets/js/main.js',
	'assets/build/js/custom.js'
);

mix.scripts(
	[
		'node_modules/jquery/dist/jquery.js',
		'node_modules/lodash/lodash.min.js',
		'node_modules/gsap/src/uncompressed/TweenMax.js',
		'node_modules/gsap/src/uncompressed/plugins/ScrollToPlugin.js',
		'node_modules/gsap/src/uncompressed/utils/SplitText.js',
		'node_modules/nprogress/nprogress.js',
		'node_modules/wowjs/dist/wow.js',
		'node_modules/tilt.js/dest/tilt.jquery.min.js',
		'node_modules/parallax-js/dist/parallax.min.js',
		'assets/js/functions.js',
		'assets/build/js/custom.js'
	],
	'public/js/app.js'
);



/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application.
 |
 */

// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.standaloneSass('src', output); <-- Faster, but isolated from Webpack.
// mix.less(src, output);
// mix.stylus(src, output);
// mix.browserSync('my-site.dev');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.options({
//   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });
