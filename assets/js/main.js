'use strict';

// Modules
import _Ajax from './modules/ajax';
import _App from './modules/app';
import _Pjax from './modules/pjax';
import _Utils from './modules/utils';

// Components
import Animate from './components/animate';
import App from './components/app';
import Breakdown from './components/breakdown';
import Loader from './components/loader';
import Modal from './components/modal';
import Pages from './components/pages';
import Scroll from './components/scroll';

// Pages
import Home from './pages/home';

// Make modules accessible
_Utils.access(_Ajax, '_Ajax');
_Utils.access(_App, '_App');
_Utils.access(_Pjax, '_Pjax');
_Utils.access(_Utils, '_Utils');

// Main
$(document).ready(function()
{
	_App.init([
		{ page: 'app', js: [App, Animate] },
	]);

	_App.registerJs([
		{ page: 'app', js: [Pages, Breakdown, Modal] },
		{ page: 'home', js: [Home] },
	]);
});
