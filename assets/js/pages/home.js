'use strict';

import App from '../components/app';
import Breakdown from '../components/breakdown';
import Modal from '../components/modal';
import PageScroller from '../components/pagescroller';
import VideoPlayer from '../components/videoplayer';

let el = {};

let _isIE = false;
let _isFF = false;
let _isMobile = false;
let _isChrome = false;
let _canPlayBackgroundVideos = true;



const init = () =>
{

	initDOM();
	initEvents();
};

const initDOM = () =>
{
	el = {};
	var scene = document.getElementById('scene');
	var parallaxInstance = new Parallax(scene, {
		relativeInput: true,
		hoverOnly: true,
	});


};

const initEvents = () =>
{

};

export default
{
	init,
};
