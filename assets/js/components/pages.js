'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initEvents();
	animApp();
};

const initDOM = () =>
{
	el = {};
};

const initEvents = () =>
{
	$(document)
	.on('click touch', '[data-prevent]', function(e) { e.preventDefault(); return false; })
	.on('click touch', '[data-go-to-top]', function(e) { scrollToTop(); });
};

const animEnd = () =>
{
};

const animStart = () =>
{
};

const animApp = () =>
{
	var tl = new TimelineMax({})
	tl.to('.background-loader', 1.5, {backgroundPosition: '1000px 100px', ease: Linear.easeNone })
	.to('.background-loader', 1.5, {backgroundPosition: '2000px 200px', ease: Linear.easeNone})
	.to('.background-loader', 1, { autoAlpha: 0 })
	.set('.background-loader',  { display: 'none'});
};

const scrollToTop = () =>
{
	TweenLite.to('body', 1, { scrollTo: 0 });
};

export default
{
	init,
	scrollTop: scrollToTop,
};

$(document).on('pjax:send', function() { animStart(); });
$(document).on('pjax:callback', function() { animEnd(); });
