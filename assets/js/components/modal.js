'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initEvents();
};

const initDOM = () =>
{
	el = {};

	el.modal = $('#modal');
	el.modal_content = $('[data-modal-content]');

	el.modal_open = $('[data-modal-open]');
	el.modal_toggle = $('[data-modal-toggle]');
	el.modal_close = $('[data-modal-close]');
};

const initEvents = () =>
{
	el.modal_open.on('click', function() { openModal(); });
	el.modal_toggle.on('click', function() { toggleModal(); });
	el.modal_close.on('click', function() { closeModal(); });

	$(document).on('keyup', function(e) { if (e.which === 27) closeModal() });
};

const toggleModal = () =>
{
	if (el.modal.hasClass('modal-opened'))
		closeModal();
	else
		openModal();
};

const openModal = () =>
{
	el.modal.addClass('active');
	setTimeout(() => { el.modal.addClass('modal-opened'); emitOpened() }, 100);
};

const closeModal = () =>
{
	el.modal.removeClass('modal-opened');
	setTimeout(() => { el.modal.removeClass('active'); emitClosed(); }, 500);
};

const emitOpened = () =>
{
	$(document).trigger('modal.opened');
};

const emitClosed = () =>
{
	$(document).trigger('modal.closed');
};

const getContent = () =>
{
	return el.modal_content;
};

export default
{
	init,
	open: openModal,
	toggle: toggleModal,
	close: closeModal,
	getContent,
};
