'use strict';

let el = {};

let _wow = null;
let _tilt = null;

const init = () =>
{
	initWow();
	initTilt();
};

const initWow = () =>
{
	if (_Utils.isNavigator.ieEdge() === true)
		return;

	_wow = new WOW({
		boxClass: 'wow-anim', // animated element css class (default is wow)
		animateClass: 'animated', // animation css class (default is animated)
		offset: 0, // distance to the element when triggering the animation (default is 0)
		mobile: false, // trigger animations on mobile devices (default is true)
		live: true, // act on asynchronously loaded content (default is true)
		callback: function(box)
		{
			// the callback is fired every time an animation is started
			// the argument that is passed in is the DOM node being animated

			// console.log('animated', box);
		},
		scrollContainer: null, // optional scroll container selector, otherwise use window
	});

	_wow.init();
};

const initTilt = () =>
{
	if (_Utils.isNavigator.ieEdge() === true)
		return;

	_tilt = $('.tilt').tilt({
		maxTilt: 20,
		perspective: 1000, // Transform perspective, the lower the more extreme the tilt gets.
		easing: 'cubic-bezier(.03,.98,.52,.99)', // Easing on enter/exit.
		scale: 1, // 2 = 200%, 1.5 = 150%, etc..
		speed: 300, // Speed of the enter/exit transition.
		transition: true, // Set a transition on enter/exit.
		disableAxis: null, // What axis should be disabled. Can be X or Y.
		reset: true, // If the tilt effect has to be reset on exit.
		glare: false, // Enables glare effect
		maxGlare: 1, // From 0 - 1.
	});
};

export default
{
	init,
};
