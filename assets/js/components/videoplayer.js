'use strict';

export default function(config)
{
	const __name = 'VideoPlayer';

	let el = {};

	let _isPlayed = false;

	let _pauseTimeout = -1;
	let _pauseTimeoutTime = 1000;

	let _config = _.assign({
		video: '',
		auto_play: true,
		debug: false,
		onPlay: () => '',
		onPause: () => '',
		onChange: () => '',
		onError: () => '',
	}, config);

	const init = () =>
	{
		if ($(_config.video).length === 0)
			return consoleWarn('invalid "video" element:');

		initDOM();
		initEvents();
		initVideoPlayer();

		return true;
	};

	const initDOM = () =>
	{
		el = {};

		el.video = $(_config.video).eq(0);
		el.video_dom = el.video.get(0);
	};

	const initEvents = () =>
	{
		el.video_dom.addEventListener('pause', emitPause);
		el.video_dom.addEventListener('error', emitError);

		$(document).one('pjax:before', destroy);
	};

	const initVideoPlayer = () =>
	{
		consoleLog('init:');

		if (_config.auto_play === true)
			play();
	};

	const destroy = () =>
	{
		el.video_dom.removeEventListener('pause', emitPause);
		el.video_dom.removeEventListener('error', emitError);

		$(document).off('pjax:before', destroy);

		pause();
	};

	const play = () =>
	{
		_isPlayed = true;

		requestAnimationFrame(checkPlay);
	};

	const checkPlay = () =>
	{
		if (isVideoPlaying() === false && isPlayed() === true) {
			el.video_dom.play();
			emitPlay();
		}

		clearTimeout(_pauseTimeout);
		_pauseTimeout = setTimeout(() => el.video_dom.pause(), _pauseTimeoutTime);

		if (isPlayed() === true)
			requestAnimationFrame(checkPlay);
	};

	const pause = () =>
	{
		_isPlayed = false;

		if (isVideoPlaying() === true) {
			el.video_dom.pause();
			emitPause();
		}
	};

	const show = () =>
	{
		el.video.setHidden(false);

		if (isVideoPlaying() === false)
			el.video_dom.play();
	};

	const hide = () =>
	{
		el.video.setHidden(true);

		if (isVideoPlaying() === true)
			el.video_dom.pause();
	};

	const setSrc = (src, callback) =>
	{
		const doCallback = () => {
			el.video_dom.removeEventListener('loadeddata', doCallback);
			if (callback) callback();
		};

		el.video_dom.addEventListener('loadeddata', doCallback, false);
		el.video_dom.setAttribute('src', src);
		el.video_dom.load();
	};

	const setAttribute = (attribute, active = true) =>
	{
		el.video_dom.setAttribute(attribute, active);
	};

	const isVideoPlaying = () =>
	{
		return el.video_dom.playing;
	};

	const isPlayed = () =>
	{
		return _isPlayed;
	};

	const onPlay = (callback) =>
	{
		_config.onPlay = callback;
	};

	const onPause = (callback) =>
	{
		_config.onPause = callback;
	};

	const onChange = (callback) =>
	{
		_config.onChange = callback;
	};

	const onError = (callback) =>
	{
		_config.onError = callback;
	};

	const emitPlay = () =>
	{
		consoleLog('play:', isPlayed());

		_config.onPlay();
	};

	const emitPause = () =>
	{
		consoleLog('pause:', isPlayed());

		_config.onPause();
	};

	const emitChange = (play) =>
	{
		consoleLog('change:', { play, });

		_config.onChange(play);
	};

	const emitError = (error) =>
	{
		consoleLog('error:', { error, });

		_config.onError(error);
	};

	const consoleLog = (...args) =>
	{
		if (_config.debug === true)
			return console.log(`${__name}:`, ...args) || false;
	};

	const consoleWarn = (...args) =>
	{
		return console.warn(`${__name}:`, ...args, _config) || false;
	};

	return init() && {
		destroy,
		setSrc,
		setAttribute,
		onPlay,
		onPause,
		onChange,
		onError,
		isVideoPlaying,
		isPlayed,
		play,
		pause,
		show,
		hide,
	};
};

Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
	get: function(){
		return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
	}
})