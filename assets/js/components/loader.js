'use strict';

let _wasEmitted = false;

let _securityTimeoutBlockLoader = -1;
const _securityTimeoutBlockLoaderTime = 1000 * 10;

let _delayBeforeSendAppReady = -1;
const _delayBeforeSendAppReadyTime = 500;

const _elementsEvents = {
	img: 'load',
	video: 'loadeddata',
	audio: 'loadeddata',
};

const _eventsSuffix = {
	load: '[src]',
	loadeddata: '[src]',
};

const start = (itemsToLoad = [], config = {}) =>
{
	config = _.assign({
		min: 0,
		max: _securityTimeoutBlockLoaderTime,
		delay: _delayBeforeSendAppReadyTime,
		callback: () => '',
	}, config);

	let itemsLoadDone = false;
	let minTimeLoaded = false;

	const emitFinished = () =>
	{
		if (_wasEmitted === true || minTimeLoaded === false) return;
			_wasEmitted = true;

		clearTimeout(_securityTimeoutBlockLoader);

		_delayBeforeSendAppReady = setTimeout(() => config.callback(), config.delay);
	};

	let elements = [];

	for (let i = 0, c = itemsToLoad.length; i < c; i++) {
		const item = itemsToLoad[i];
		const eventName = _elementsEvents[item];
		const eventSuffix = _eventsSuffix[eventName] || '';

		if (eventName === undefined)
			continue;

		elements.push(...document.querySelectorAll(item + eventSuffix));
	}

	// Load elements
	ready(elements, () => { itemsLoadDone = true; emitFinished(); });

	// Minimum time loader
	setTimeout(() => { minTimeLoaded = true; if (itemsLoadDone === true) emitFinished(); }, config.min);

	// Security timeout
	_securityTimeoutBlockLoader = setTimeout(() => { emitFinished(); }, config.max);
};

const ready = function (items, callback)
{
	const wasSingle = items instanceof HTMLElement;

	if (wasSingle === true)
		items = [items];

	let itemsLoadedCount = 0;
	let itemsToLoadCount = items.length;

	const emitFinished = () =>
	{
		callback(wasSingle ? items[0] : items);
	};

	const checkFinished = () =>
	{
		itemsLoadedCount += 1;

		if (itemsLoadedCount === itemsToLoadCount)
			emitFinished();
	};

	for (let i = 0, c = itemsToLoadCount; i < c; i++) {
		const item = items[i];
		const itemEvent = _elementsEvents[item.tagName.toLowerCase()];

		if (itemEvent === undefined) {
			itemsLoaded += 1;
			console.warn('Loader: undefined event for element:', item);
		}

		if (isItemLoaded(item) === true) checkFinished();
		else item.addEventListener(itemEvent, function() { checkFinished(); });
	}
};

const isItemLoaded = function (item)
{
	const itemTagName = item.tagName.toLowerCase();

	if (itemTagName === 'video' || itemTagName === 'audio') {
		return item.readyState !== undefined && item.readyState === 4;
	}
	else if (itemTagName === 'img') {
		return item.complete !== undefined && item.complete === true;
	}

	return true;
};

export default
{
	start,
	ready,
	isItemLoaded,
};
