'use strict';

let el = {};

let _version = 0;

const init = () =>
{
	_version = Blade.version;

	initDOM();
	initEvents();
	initAjax();
};

const initDOM = () =>
{
	el = {};
};

const initEvents = () =>
{
	$(document).on('loader.ready', function() { initApp(); });
	$(document).on('pjax:callback', function() { checkVersion(); });
};

const initAjax = () =>
{
	_Ajax.setPrefix('ajax');
	_Ajax.setVersion(_version);
};

const initApp = () =>
{

};

const goTop = () =>
{
	$('body').scrollTop(0);
};

const checkVersion = () =>
{
	if (Blade.version !== _version) _pages.showNewVersionModal();
};

export default
{
	init,
};
