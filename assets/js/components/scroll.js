'use strict';

const _keys = [32, 33, 34, 35, 36, 38, 40];

let _callback = null;

const preventDefault = (e) =>
{
	e = e || window.event;

	if (e.preventDefault)
		e.preventDefault();

	e.returnValue = false;

	if (_callback)
		_callback(e);
}

const preventDefaultKeys = (e) =>
{
	if (_keys.indexOf(e.keyCode) === -1)
		return;

	preventDefault(e);
	return false;
}

const disable = (callback) =>
{
	if (window.addEventListener)
		window.addEventListener('DOMMouseScroll', preventDefault, false);

	window.onwheel = preventDefault;
	window.onmousewheel = document.onmousewheel = preventDefault;
	window.ontouchmove  = preventDefault;

	document.onkeydown  = preventDefaultKeys;

	on(callback);
}

const enable = () =>
{
	if (window.removeEventListener)
		window.removeEventListener('DOMMouseScroll', preventDefault, false);

	window.onmousewheel = document.onmousewheel = null;
	window.onwheel = null;
	window.ontouchmove = null;

	document.onkeydown = null;
}

const on = (callback) =>
{
	if (callback !== undefined) _callback = callback;
}

const off = () =>
{
	_callback = null;
}

export default
{
	disable,
	enable,
	off,
	on,
};
