'use strict';

import Breakdown from './breakdown';
import Scroll from './scroll';

export default function(config)
{
	let el = {};

	let _isLocked = false;
	let _isEnabled = true;

	let _tickDir = '';
	let _pixelTotal = 0;

	let _oldDeltaY = 0;
	let _maxDeltaY = 150;

	let _windowHeight = null;
	let _resetTimeout = -1;

	let _config = _.assign({
		scroll: '',
		panels: '',
		class: 'active',
		duration: .8,
		distance: 400,
		init: false,
		min_height: false,
		active: true,
		debug: false,
		onChange: () => '',
		onEnable: () => '',
	}, config);

	const init = () =>
	{
		if ($(_config.scroll).length === 0)
			return consoleWarn('PageScroller: invalid "scroll" element:');

		if ($(_config.panels).length === 0)
			return consoleWarn('PageScroller: invalid "panels" element:');

		initDOM();
		initEvents();
		initPageScroller();

		return true;
	};

	const initDOM = () =>
	{
		el = {};

		el.scroll = $(_config.scroll);
		el.panels = $(_config.panels);
	};

	const initEvents = () =>
	{
		$(window).on('resize', resizeWindow);
		$(document).one('pjax:before', destroy);
	};

	const initPageScroller = () =>
	{
		consoleLog('init:');

		// Enable Scroller
		enableScroller(_config.active);

		// Init window height
		resizeWindow();

		// Init panel position
		if (isEnabled() === true && _config.init !== false)
			setTimeout(() => moveToPanel(el.panels.eq(_config.init)), 330);
		else
			el.panels.eq(_config.init).addClass(_config.class);
	};

	const destroy = () =>
	{
		$(window).off('resize', resizeWindow);
		$(document).off('pjax:before', destroy);

		enable(false);
	};

	const resizeWindow = () =>
	{
		if (isActive() === false)
			return;

		const windowHeight = $(window).height();
		const minHeight = _config.min_height;

		if (_windowHeight === null)
			_windowHeight = windowHeight;

		if (_config.min_height !== false) {
			if (windowHeight < minHeight && _windowHeight >= minHeight) {
				enableScroller(false);
			}
			else if (windowHeight >= minHeight && _windowHeight < minHeight) {
				enableScroller();
			}
		}

		_windowHeight = windowHeight;
	};

	const shouldChangePanel = (e) =>
	{
		if (isEnabled() === false || lock() === true)
			return;

		setTimeout(() => {
			let deltaY = Math.min(_maxDeltaY, normalizeWheel(e).pixelY);
			const tickDir = deltaY === 0 ? '' : deltaY > 0 ? 'down' : 'up';

			deltaY = Math.abs(deltaY);
			consoleLog('scroll:', { deltaY, tickDir, deltaY, _pixelTotal, e });

			if (tickDir === '' || (tickDir !== '' && tickDir !== _tickDir) || _oldDeltaY > deltaY)
				_pixelTotal = 0;

			_oldDeltaY = deltaY;
			_tickDir = tickDir;
			_pixelTotal += deltaY;

			if (_pixelTotal > _config.distance) {
				if (_tickDir === 'down') nextPanel();
				else prevPanel();

				_tickDir = '';
				_pixelTotal = 0;
			}

			clearTimeout(_resetTimeout);
			_resetTimeout = setTimeout(() => _pixelTotal = 0, _config.duration * 1000);
		});
	};

	const currentPanel = () =>
	{
		const $panel = el.panels.filter('.active').eq(0);

		moveToPanel($panel, $panel);
	};

	const prevPanel = () =>
	{
		const $panel = el.panels.filter('.active').eq(0);

		moveToPanel($panel.prev(_config.panels), $panel);
	};

	const nextPanel = () =>
	{
		const $panel = el.panels.filter('.active').eq(0);

		moveToPanel($panel.next(_config.panels), $panel);
	};

	const moveToPanel = ($panel, $oldPanel) =>
	{
		if (isEnabled() === false || lock() === true)
			return;

		if ($panel === undefined || $panel.length === 0)
			return consoleWarn('PageScroller: invalid "panel" element:', $panel);

		const panelIndex = $panel.index(_config.panels);
		const scrollPos = panelIndex * _windowHeight;

		TweenLite.set(el.scroll, { scrollTo: el.scroll.scrollTop() });
		TweenLite.to(el.scroll, _config.duration, { scrollTo: scrollPos });

		el.panels.removeClass(_config.class);
		$panel.addClass(_config.class);

		lock(true);
		emitChange(panelIndex, $panel, $oldPanel);
	};

	const lock = (isLocked) =>
	{
		if (isLocked !== undefined) {
			_isLocked = isLocked;
			setTimeout(() => _isLocked = false, _config.duration * 1000);
		}

		return _isLocked;
	};

	const enableScroller = (enabled = true) =>
	{
		_isEnabled = enabled;

		if (isEnabled() === true) {
			Scroll.disable(shouldChangePanel);

			if ([window, document, 'html'].indexOf(_config.scroll) !== -1)
				$('body').css('overflow', 'hidden');
			else
				el.scroll.css('overflow', 'hidden');
		}
		else {
			Scroll.enable();

			if ([window, document, 'html'].indexOf(_config.scroll) !== -1)
				$('body').css('overflow', '');
			else
				el.scroll.css('overflow', '');
		}

		emitEnable(enabled);
	};

	const isEnabled = (withConfig = true) =>
	{
		if (withConfig === true && isActive() === false)
			return false;

		return _isEnabled;
	};

	const isActive = () =>
	{
		return _config.active;
	};

	const onChange = (callback) =>
	{
		_config.onChange = callback;
	};

	const onEnable = (callback) =>
	{
		_config.onEnable = callback;
	};

	const emitChange = (panelIndex, $panel, $oldPanel) =>
	{
		consoleLog('change:', { panelIndex, $panel, $oldPanel, });

		_config.onChange(panelIndex, $panel, $oldPanel);
	};

	const emitEnable = (enabled) =>
	{
		consoleLog('enable:', { enabled, });

		_config.onEnable(enabled);
	};

	const consoleLog = (...args) =>
	{
		if (_config.debug === true)
			return console.log('PageScroller:', ...args) || false;
	};

	const consoleWarn = (...args) =>
	{
		return console.warn(...args, _config) || false;
	};

	const enable = (enabled) =>
	{
		_config.active = enabled;

		enableScroller(enabled);

		if (enabled === true)
			currentPanel();
	};

	return init() && {
		prevPanel,
		nextPanel,
		onChange,
		onEnable,
		isEnabled,
		enable,
		destroy,
	};
};

/*
 * Normalize scroll
 * https://stackoverflow.com/a/30134826
 */

// Reasonable defaults
var PIXEL_STEP  = 10;
var LINE_HEIGHT = 40;
var PAGE_HEIGHT = 800;

function normalizeWheel(/*object*/ event) /*object*/ {
  var sX = 0, sY = 0,       // spinX, spinY
      pX = 0, pY = 0;       // pixelX, pixelY

  // Legacy
  if ('detail'      in event) { sY = event.detail; }
  if ('wheelDelta'  in event) { sY = -event.wheelDelta / 120; }
  if ('wheelDeltaY' in event) { sY = -event.wheelDeltaY / 120; }
  if ('wheelDeltaX' in event) { sX = -event.wheelDeltaX / 120; }

  // side scrolling on FF with DOMMouseScroll
  if ( 'axis' in event && event.axis === event.HORIZONTAL_AXIS ) {
    sX = sY;
    sY = 0;
  }

  pX = sX * PIXEL_STEP;
  pY = sY * PIXEL_STEP;

  if ('deltaY' in event) { pY = event.deltaY; }
  if ('deltaX' in event) { pX = event.deltaX; }

  if ((pX || pY) && event.deltaMode) {
    if (event.deltaMode == 1) {          // delta in LINE units
      pX *= LINE_HEIGHT;
      pY *= LINE_HEIGHT;
    } else {                             // delta in PAGE units
      pX *= PAGE_HEIGHT;
      pY *= PAGE_HEIGHT;
    }
  }

  // Fall-back if spin cannot be determined
  if (pX && !sX) { sX = (pX < 1) ? -1 : 1; }
  if (pY && !sY) { sY = (pY < 1) ? -1 : 1; }

  return { spinX  : sX,
           spinY  : sY,
           pixelX : pX,
           pixelY : pY };
}
